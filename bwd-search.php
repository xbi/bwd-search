<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://xbonell.com
 * @since             1.0.0
 * @package           Bwd_Search
 *
 * @wordpress-plugin
 * Plugin Name:       BWD Search
 * Plugin URI:        https://bitbucket.org/xbi/bwd-search
 * Description:       Provides trip finder functionality for the <a href="https://www.travelbwd.com" target="_blank" rel="noopener nofollow">Travel BWD</a> website.
 * Version:           1.0.0
 * Author:            Xavier Bonell
 * Author URI:        https://xbonell.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bwd-search
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'BWD_SEARCH_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-bwd-search-activator.php
 */
function activate_bwd_search() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bwd-search-activator.php';
	Bwd_Search_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-bwd-search-deactivator.php
 */
function deactivate_bwd_search() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bwd-search-deactivator.php';
	Bwd_Search_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_bwd_search' );
register_deactivation_hook( __FILE__, 'deactivate_bwd_search' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-bwd-search.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_bwd_search() {

	$plugin = new Bwd_Search();
	$plugin->run();

}
run_bwd_search();
