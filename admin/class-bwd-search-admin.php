<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://xbonell.com
 * @since      1.0.0
 *
 * @package    Bwd_Search
 * @subpackage Bwd_Search/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Bwd_Search
 * @subpackage Bwd_Search/admin
 * @author     Xavier Bonell <mail@xbonell.com>
 */
class Bwd_Search_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bwd_Search_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bwd_Search_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bwd-search-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bwd_Search_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bwd_Search_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bwd-search-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function display_admin_page() {

		add_options_page(
			__( 'BWD Search settings', 'bwd-search' ), // $page_title
			__( 'BWD Search', 'bwd-search' ), // $menu_title
			'manage_options', // $capability
			'bwd-search-settings', // $menu_slug
			array($this, 'load_admin_template') // $function
		);

	}

	public function load_admin_template() {
		include_once 'partials/bwd-search-admin-display.php';
	}

}


function bwd_search_plugin_options_validate( $input ) {
	$newinput['api_token'] = trim( $input['api_token'] );
	$newinput['root_path'] = trim( $input['root_path'] );
	$newinput['search_path'] = trim( $input['search_path'] );
	$newinput['single_path'] = trim( $input['single_path'] );
	$newinput['experiences_path'] = trim( $input['experiences_path'] );
	$newinput['show_datepicker'] = trim( $input['show_datepicker'] );
    return $newinput;
}

function bwd_search_settings_text() {
    echo '<p>' . __('Here you can set the authentication token for the <strong>TTC API <small>v4</small></strong> and the paths to all the plugin related pages.', 'bwd-search') . '</p>';
}

function bwd_search_setting_api_token() {
    $options = get_option( 'bwd_search_plugin_options' );
    echo "<input id='bwd_search_setting_api_token' name='bwd_search_plugin_options[api_token]' type='text' value='" . esc_attr( $options['api_token'] ) . "' />";
}

function bwd_search_setting_root_path() {
    $options = get_option( 'bwd_search_plugin_options' );
    echo "<input id='bwd_search_setting_root_path' name='bwd_search_plugin_options[root_path]' type='text' value='" . esc_attr( $options['root_path'] ) . "' placeholder='Default: /' />";
}

function bwd_search_setting_search_path() {
    $options = get_option( 'bwd_search_plugin_options' );
    echo "<input id='bwd_search_setting_search_path' name='bwd_search_plugin_options[search_path]' type='text' value='" . esc_attr( $options['search_path'] ) . "' placeholder='Default: /trips' />";
}

function bwd_search_setting_single_path() {
    $options = get_option( 'bwd_search_plugin_options' );
    echo "<input id='bwd_search_setting_single_path' name='bwd_search_plugin_options[single_path]' type='text' value='" . esc_attr( $options['single_path'] ) . "' placeholder='Default: /trips/trip' />";
}

function bwd_search_setting_experiences_path() {
    $options = get_option( 'bwd_search_plugin_options' );
    echo "<input id='bwd_search_setting_experiences_path' name='bwd_search_plugin_options[experiences_path]' type='text' value='" . esc_attr( $options['experiences_path'] ) . "' placeholder='Default: /experiences' />";
}

function bwd_search_setting_show_datepicker() {
	$options = get_option( 'bwd_search_plugin_options' );
	
	$html = "<input id='bwd_search_setting_show_datepicker' name='bwd_search_plugin_options[show_datepicker]' type='checkbox'  value='1'" . checked( 1, $options['show_datepicker'], false ) . "' />";
	$html .= "<label for='bwd_search_setting_show_datepicker'>Show the date picker</label>";

	echo $html;
}

function bwd_search_register_settings() {
    register_setting( 'bwd_search_plugin_options', 'bwd_search_plugin_options', 'bwd_search_plugin_options_validate' );
    add_settings_section( 'api_settings', __('API & Plugin Settings', 'bwd-search'), 'bwd_search_settings_text', 'bwd_search_plugin' );
	add_settings_field( 'bwd_search_setting_api_key', __('API Token', 'bwd-search'), 'bwd_search_setting_api_token', 'bwd_search_plugin', 'api_settings' );
    add_settings_field( 'bwd_search_setting_root_path', __('Path to the site\'s homepage', 'bwd-search'), 'bwd_search_setting_root_path', 'bwd_search_plugin', 'api_settings' );
	add_settings_field( 'bwd_search_setting_search_path', __('Path to the "Trip Finder" search results page', 'bwd-search'), 'bwd_search_setting_search_path', 'bwd_search_plugin', 'api_settings' );
	add_settings_field( 'bwd_search_setting_single_path', __('Path to the trip single page', 'bwd-search'), 'bwd_search_setting_single_path', 'bwd_search_plugin', 'api_settings' );
	add_settings_field( 'bwd_search_setting_experiences_path', __('Path to the "Experiences" page', 'bwd-search'), 'bwd_search_setting_experiences_path', 'bwd_search_plugin', 'api_settings' );
	add_settings_field( 'bwd_search_setting_show_datepicker', __('Show date picker in the trip finder form?', 'bwd-search'), 'bwd_search_setting_show_datepicker', 'bwd_search_plugin', 'api_settings');
}

add_action( 'admin_init', 'bwd_search_register_settings' );