<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://xbonell.com
 * @since      1.0.0
 *
 * @package    Bwd_Search
 * @subpackage Bwd_Search/admin/partials
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap bwd-wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
    <form action="options.php" method="post">
        <?php 
            settings_fields( 'bwd_search_plugin_options' );
            do_settings_sections( 'bwd_search_plugin' );
        ?>
        <hr />
        <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save "BWD Search" settings', 'bwd-search' ); ?>" />
    </form>
</div>
