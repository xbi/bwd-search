<?php

/**
 * Fired during plugin activation
 *
 * @link       https://xbonell.com
 * @since      1.0.0
 *
 * @package    Bwd_Search
 * @subpackage Bwd_Search/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Bwd_Search
 * @subpackage Bwd_Search/includes
 * @author     Xavier Bonell <mail@xbonell.com>
 */
class Bwd_Search_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
