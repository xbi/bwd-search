<?php
/**
 * Template Name: Get Full Trips Include
 *
 * Get all trips from the selected brands and merge them into one data set with enhanced info.
 *
 * @package Bwd_Search
 */

function arrayPushAssoc($array, $key, $value) {
  $array[$key] = $value;
  return $array;
}

function getTrip($trip, $region) {
  $countriesVisited = array();
  $extraImages = array();
  $locationsVisited = array();
  $limit = 0;

  $args = array(
    'headers' => array(
      'Authorization' => 'Basic ' . base64_encode('token:' . get_option( 'bwd_search_plugin_options' )['api_token']),
      'Accept' => 'application/vnd.ttc.v4+json'
    )
  );

  $url = 'https://api.ttc.com/brands/' . $trip['brand'] . '/tours/' . $trip['id'] . '?regions=' . $region;
  $request = wp_remote_get($url, $args);
  
  error_log( print_r($url, true) );

  if( is_wp_error($request) ){
    return false;
  }

  $body = wp_remote_retrieve_body($request); 
  $data = json_decode($body, TRUE);
  $limit = floatval(wp_remote_retrieve_header($request, 'X-RateLimit-Remaining'));

  if ($limit < 50) {
    sleep(1);
  }

  if ( !empty($data['tourOptions']) ) {

    foreach ($data['tourOptions'] as $option) {

      if ( !empty($option['seasons']) ) {

        foreach ($option['seasons'] as $season) {

          foreach($season['content'] as $content) {
            $countries = $content['countriesVisited'];
            $itinerary = $content['itinerary'];
            $images = array_filter($content['images'], function ($image) { return $image['type'] == 'photo'; });
            
            array_push($extraImages, $images);

            foreach ($countries as $country) {
              array_push($countriesVisited, $country['name']);
            }
            unset($country);
  
            foreach ($itinerary as $itineraryItem) {
              foreach ($itineraryItem['locationsVisited'] as $location) {
                array_push($locationsVisited, $location['name']);
              }
              unset($location);
            }
            unset($itineraryItem);
          }
          unset($content);

        }
        unset($season);

      }

    }
    unset($option);
  }

  $countriesVisited = array_unique($countriesVisited);
  $extraImages = array_unique($extraImages);
  $locationsVisited = array_unique($locationsVisited);

  $trip = arrayPushAssoc($trip, 'countriesVisited', $countriesVisited);
  $trip = arrayPushAssoc($trip, 'extraImages', $extraImages);
  $trip = arrayPushAssoc($trip, 'locationsVisited', $locationsVisited);

  return $trip;
}

function getFullTrips($trips, $region) {
  $i=0;

  foreach ($trips as $trip) {
    $trips[$i] = getTrip($trip, $region);
    $i++;
  }
  unset($trip);

  return $trips;
}

function getTrips() {
  $force = ( isset($_GET['action']) && ( $_GET['action'] == 'refresh' ) );
  $region = isset($_GET['region']) ? $_GET['region'] : 'us';
  $transient = get_transient( 'bwd_trips_' . $region );

  if( !empty( $transient ) && !$force ) {
    return $transient;
  } else {
    delete_transient( 'bwd_trips_' . $region );
    $brands = array( 'costsaver', 'insightvacations', 'luxurygold', 'trafalgar' );
    $trips = array();
 
    $args = array(
      'headers' => array(
        'Authorization' => 'Basic ' . base64_encode('token:' . get_option( 'bwd_search_plugin_options' )['api_token']),
        'Accept' => 'application/vnd.ttc.v4+json'
      )
    );

    foreach ($brands as $brand) {
      $request = wp_remote_get('https://api.ttc.com/brands/' . $brand . '?regions=' . $region, $args);

      if( is_wp_error($request) ){
        return false;
      }
  
      $body = wp_remote_retrieve_body($request);
      $data = json_decode($body, TRUE);
      $trips = array_merge($trips, $data);
    }
    unset($brand);

    $trips = getFullTrips($trips, $region);

    $encodedTrips = json_encode($trips);
    set_transient( 'bwd_trips_' . $region , $encodedTrips );   // Save the API response so we don't have to call again until later (600 seconds).
    return $encodedTrips;
  }
}

function getTripsCached() {
  $region = isset($_GET['region']) ? $_GET['region'] : 'us';
  $transient = get_transient( 'bwd_trips_' . $region );  
  if( !empty( $transient ) ) {
    return $transient;
  } else {
    return json_encode(array());
  }
}