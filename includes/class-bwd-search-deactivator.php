<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://xbonell.com
 * @since      1.0.0
 *
 * @package    Bwd_Search
 * @subpackage Bwd_Search/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bwd_Search
 * @subpackage Bwd_Search/includes
 * @author     Xavier Bonell <mail@xbonell.com>
 */
class Bwd_Search_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
