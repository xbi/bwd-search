import React, { useCallback, useEffect, useState } from 'react';
import dayjs from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';
import { CSSTransition } from 'react-transition-group';
import { get } from '../../utils/apiUtils';
import { sortOptions } from '../../utils/config';
import getParams from '../../utils/getParams';
import { useRegion } from '../../utils/useRegion';
import TripFinderHeader from '../../components/tripFinderHeader';
import TripFinderExperiences from '../../components/tripFinderExperiences';
import TripFinderFilters from '../../components/tripFinderFilters';
import TripFinderTours from '../../components/tripFinderTours';

dayjs.extend(isBetween);
dayjs.extend(isSameOrAfter);
dayjs.extend(isSameOrBefore);

const App = ({ experiencesPath, singlePath }) => {
    const { getRegion } = useRegion();
    const currentRegion = getRegion();

    const [appState, setAppState] = useState({
        loading: false,
        showFilters: false
    });

    const [filters, setFilters] = useState({
        brands: [],
        destination: [],
        durationRange: [],
        from: null,
        priceRange: [],
        sort: sortOptions[0], // Default: Sort by Price
        to: null
    });

    const [fullExperiences, setFullExperiences] = useState([]);
    const [fullTrips, setFullTrips] = useState([]);
    const [experiencesResults, setExperiencesResults] = useState([]);
    const [searchResults, setSearchResults] = useState([]);

    const toggleFilters = status => setAppState({ ...appState, showFilters: status });

    const filterExperiences = useCallback((experiences = fullExperiences) => {
        const { destination } = filters;

        // Filter by destination
        const includesDestination = experience => {
            if (destination.length > 0) {
                const route = experience.acf.route;
                const countries = experience.acf.countries;
                let isIncluded = false;
                destination.forEach(item => {
                    isIncluded = route.indexOf(item) !== -1 || countries.indexOf(item) !== -1;
                });
                return isIncluded;
            } else {
                return true;
            }
        };

        const filteredExperiences = [...experiences]
            .filter(experience => includesDestination(experience));

        setExperiencesResults([...filteredExperiences]);
    }, [filters, fullExperiences]);

    const filterTrips = useCallback((trips = fullTrips) => {
        const { brands, destination, durationRange, from, priceRange, sort, to } = filters;

        // Filter trips by brand
        const hasBrand = trip => brands.length > 0 ? brands.includes(trip.brand) : true;

        // Filter by destination
        const includesDestination = trip => {
            if (destination.length > 0) {
                const startPlace = trip.content[0].startPlace.country;
                const endPlace = trip.content[0].endPlace.country;
                const countriesVisited = Object.keys(trip.countriesVisited).map((index) => trip.countriesVisited[index]);
                const locationsVisited = Object.keys(trip.locationsVisited).map((index) => trip.locationsVisited[index]);
                return (
                    destination.includes(startPlace) ||
                    destination.includes(endPlace) ||
                    destination.some(country => countriesVisited.includes(country)) ||
                    destination.some(location => locationsVisited.includes(location))
                );
            } else {
                return true;
            }
        };

        // Filter by departure date
        const isInDateRange = trip => {
            const firstDepartureDate = trip.content[0].firstDepartureDate;
            const lastDepartureDate = trip.content[0].lastDepartureDate;
            if (from) {
                return dayjs(from).isBetween(firstDepartureDate, lastDepartureDate, null, '[]');
            } else if (to) {
                return dayjs(to).isBetween(firstDepartureDate, lastDepartureDate, null, '[]');
            } else if (from && to) {
                return dayjs(to).isBetween(firstDepartureDate, lastDepartureDate, null, '[]') && dayjs(from).isBetween(firstDepartureDate, lastDepartureDate, null, '[]');
            } else {
                return true;
            }
        };

        // Filter by duration range
        const isInDurationRange = trip => {
            if (durationRange.length > 0) {
                const duration = trip.content[0].durationMin;
                return durationRange.filter(item => duration >= item[0] && duration <= item[1]).length > 0;
            } else {
                return true;
            }
        }

        // Filter by price range
        const isInPriceRange = trip => {
            if (priceRange.length > 0) {
                const price = trip.content[0].fromPrice.price.adultPrice;
                return priceRange.filter(item => price >= item[0] && price <= item[1]).length > 0;
            } else {
                return true;
            }
        }

        // Sort By 
        const sortBy = (options) => {
            const { value, order } = options;

            return (a, b) => {
                let varA;
                let varB;
            
                switch (value) {
                    case 'duration':
                        varA = a.content[0].durationMin;
                        varB = b.content[0].durationMin;
                        break;
                    case 'name':
                        varA = a['name'].toUpperCase();
                        varB = b['name'].toUpperCase();
                        break;
                    case 'price':
                        varA = a.content[0].fromPrice.price.adultPrice;
                        varB = b.content[0].fromPrice.price.adultPrice;
                        break;    
                    default:
                        break;
                }

                let comparison = 0;
                
                if (varA > varB) {
                    comparison = 1;
                } else if (varA < varB) {
                    comparison = -1;
                }
                
                return (
                    (order === 'desc') ? (comparison * -1) : comparison
                );
            };
        }

        const filteredTrips = [...trips]
            .filter(trip => hasBrand(trip))
            .filter(trip => includesDestination(trip))
            .filter(trip => isInDateRange(trip))
            .filter(trip => isInDurationRange(trip))
            .filter(trip => isInPriceRange(trip))
            .sort(sortBy(sort));

        setSearchResults([...filteredTrips]);
    }, [filters, fullTrips]);

    useEffect(()=>{
        if (window.location.search) {
            setFilters(filters => Object.assign(filters, getParams()));            
        }
    }, []);

    useEffect(() => {
        async function fetchTripsAndExperiences() {
            const experiences = await get(`/wp-content/plugins/bwd-search/services/getExperiences.php`);
            setFullExperiences([...experiences.data]);
            const trips = await get(`/wp-content/plugins/bwd-search/services/getTripsCached.php?region=${currentRegion}`);
            setFullTrips([...trips.data]);
            setAppState(appState => ({...appState, loading: false }));
        }

        setAppState(appState => ({...appState, loading: true }));
        fetchTripsAndExperiences();
    }, [currentRegion, setAppState, setFullTrips]);

    useEffect(() => {
        filterTrips();
        filterExperiences();
    }, [filters, filterExperiences, filterTrips, setFilters]);

    return (
        <>
            <TripFinderHeader
                destination={filters.destination}
                isLoading={appState.loading}
                totalExperiences={experiencesResults.length}
                totalTrips={searchResults.length} />
            <CSSTransition
                in={experiencesResults.length > 0}
                timeout={200}
                classNames="fade"
                unmountOnExit
            >
                <div className="bwd-experiences-wrap">
                    <TripFinderExperiences
                        experiences={experiencesResults}
                        experiencesPath={experiencesPath}
                    />
                </div>
            </CSSTransition>
            <CSSTransition
                in={searchResults.length > 0}
                timeout={200}
                classNames="fade"
                unmountOnExit
            >
                <div className="bwd-tours-wrap">
                    <TripFinderTours
                        isLoading={appState.loading}
                        toggleFilters={toggleFilters}
                        tours={searchResults}
                        singlePath={singlePath}
                        filters={filters}
                        setFilters={setFilters}
                    />
                    <TripFinderFilters
                        filters={filters}
                        setFilters={setFilters}
                        showFilters={appState.showFilters}
                        toggleFilters={toggleFilters}
                        totalResults={searchResults.length}
                    />
                </div>
            </CSSTransition>    
        </>
    )
};

export default App;
