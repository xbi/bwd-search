import { DireflowComponent } from 'direflow-component';
import App from './App';

export default DireflowComponent.create({
  component: App,
  configuration: {
    tagname: 'bwd-trip-finder',
    useShadow: false
  },
  properties: {
    experiencesPath: '/experiences',
    singlePath: '/trips/trip'
  }
});