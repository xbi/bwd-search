import React from 'react';
import { useRegion } from '../../utils/useRegion';

const App = ({ rootPath }) => {
  const { currentRegion, getRegion, setRegion, supportedRegions } = useRegion();

  return (
    <div className="bwd-rs">
      <button className="bwd-rs__btn" type="button" id="bwd-rs-button">{currentRegion.currency}</button>
      <div className="bwd-rs__menu" aria-labelledby="bwd-rs-button">
        {supportedRegions.map((region, index) => {
          return (
            region.code !== getRegion() && <span className="bwd-rs__item" key={index} onClick={() => { setRegion(region.code) }}>{region.currency}</span>
          )
        })}
      </div>
    </div>
  );
};

export default App;
