import React, { useEffect, useRef, useState } from 'react';
import DatePicker, { registerLocale } from "react-datepicker";
import en from 'date-fns/locale/en-GB';
import { Typeahead } from 'react-bootstrap-typeahead';
import withLoading from '../../components/withLoading';
import { get } from '../../utils/apiUtils';
import { formatDate } from '../../utils/filters';
import getParams from '../../utils/getParams';
import { useRegion } from '../../utils/useRegion';

// Set locale for the datepicker component
registerLocale('en-GB', en);

const App = ({ searchPath, showDatepicker }) => {
    const { getRegion } = useRegion();
    const currentRegion = getRegion();
    const TypeaheadWithLoading = withLoading(Typeahead);
    const btnRef = useRef();

    const [appState, setAppState] = useState({
        loading: false,
        destinations: []
    });

    const [selectedDestinations, setSelectedDestinations] = useState(() => {
        return getParams().destination ? [...getParams().destination] : [];
    });
    const [fromDate, setFromDate] = useState(() => {
        return getParams().from ? new Date(getParams().from) : null;
    });

    const getFirstDayOfMonth = (date = new Date()) => {
        return new Date(date.getFullYear(), date.getMonth(), 1);
    };

    useEffect(() => {
        setAppState({ loading: true });
        get(`/wp-content/plugins/bwd-search/services/getDestinations.php?region=${currentRegion}`).then(destinations => {
          const rawDestinations = destinations.data;
          const destinationsOptions = rawDestinations.map(({ name }) => ({ label: name, value: name }));
          setAppState({ loading: false, destinations: destinationsOptions });
        }).catch(e => {
            console.error(e);
        });
    }, [currentRegion, setAppState]);

    const handleSearch = () => {
        const paramsObj = {
            destination: selectedDestinations.map(country => country.label),
            from: formatDate(fromDate, 'YYYY-MM-DD')
        };
        
        const searchParams = new URLSearchParams(paramsObj).toString();

        window.location.href = `${searchPath}?${searchParams}`;
    };

    const handleKeyDown = (e) => {
        if (e.keyCode === 13) {
            btnRef.current.focus();
        }
    };

    const handleSubmit = e => {
        e.preventDefault();
        handleSearch();
    };

    return (
        <form className="bwd-form" onSubmit={handleSubmit}>
            <div className="bwd-form__field bwd-form__field--destination">
                <TypeaheadWithLoading
                    id="bwd-form-destination"
                    isLoading={appState.loading}
                    labelKey="label"
                    options={appState.destinations}
                    selected={selectedDestinations}
                    onChange={setSelectedDestinations}
                    placeholder="Choose Destination"
                    selectHintOnEnter
                    onKeyDown={handleKeyDown}
                />
            </div>
            { showDatepicker &&
                <div className="bwd-form__field bwd-form__field--datepicker">
                    <DatePicker
                        selected={fromDate}
                        onChange={date => setFromDate(getFirstDayOfMonth(date))}                  
                        placeholderText="When"
                        locale="en-GB"
                        dateFormat="MMM. yyyy"
                        showMonthYearPicker
                    />
                </div>
            }
            <button type="submit" className="bwd-form__submit" ref={btnRef}>Search</button>
        </form>
    )
}

export default App;
