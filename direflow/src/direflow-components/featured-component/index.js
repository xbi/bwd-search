import { DireflowComponent } from 'direflow-component';
import App from './App';

export default DireflowComponent.create({
  component: App,
  configuration: {
    tagname: 'bwd-featured',
    useShadow: false
  },
  properties: {
    singlePath: '/trips/trip'
  }
});