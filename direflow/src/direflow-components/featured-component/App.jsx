import React, { useEffect, useState } from 'react';
import { get } from '../../utils/apiUtils';
import { useRegion } from '../../utils/useRegion';
import Featured from '../../components/featured';
import withLoading from '../../components/withLoading';

const App = ({ singlePath, num = 3 }) => {
  const { getRegion } = useRegion();
  const FeaturedWithLoading = withLoading(Featured);

  const currentRegion = getRegion();

  const [appState, setAppState] = useState({
    loading: false,
    trips: [],
    featured: []
  });

  // Get n random trips from n different brands
  const getFeaturedTrips = (trips, number) => {
    const randomTrips = [];
    do {
      const randomTrip = trips.sort(() => 0.5 - Math.random()).pop();
      if (!randomTrips.map(item => item.brand).includes(randomTrip.brand)) {
        randomTrips.push(randomTrip);
      }
    } while (randomTrips.length < number);
    return randomTrips;
  };

  useEffect(() => {
    setAppState(appState => ({...appState, loading: true }));
    get(`/wp-content/plugins/bwd-search/services/getTrips.php?region=${currentRegion}`).then(trips => {
      const allTrips = trips.data;
      const featuredTrips = getFeaturedTrips(allTrips, num);
      setAppState(appState => ({...appState, loading: false, trips: allTrips, featured: featuredTrips }));
    }).catch(e => {
        console.error(e);
    });
  }, [currentRegion, num, setAppState]);

  return (
    <FeaturedWithLoading
      isLoading={appState.loading}
      trips={appState.featured}
      singlePath={singlePath}
    />
  );
};

export default App;
