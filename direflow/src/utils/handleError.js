// handleError.js - Common Error Handler Function
export default (error) => {
    const { status, message } = error;
    switch (status) {
        case 401:
            console.error('Authenticacion error!');
            break;
        case 403:
            console.error('Authorization error!');
            break;
        case 500:
            console.error('Server error!');
            break;
        default:
            console.error('Oops!');
    }
    return message;
};