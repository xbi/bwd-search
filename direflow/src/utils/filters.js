import dayjs from 'dayjs';
import { Html5Entities } from 'html-entities';
import Numeral from 'numeral';

export const decodeHtmlEntities = htmlString => {
    const htmlEntities = new Html5Entities();
    return htmlEntities.decode(htmlString);
};

export const formatDate = (date, format = 'DD-MM-YYYY') => {
    return date ? dayjs(date).format(format) : '';
}

export const formatNumber = num => {
  return Numeral(num).format('0,0[.]00');
}

export const getBrand = brand => {
    switch (brand) {
        case 'costsaver':
            return 'Costsaver';
        case 'insightvacations':
            return 'Insight Vacations';
        case 'luxurygold':
            return 'Luxury Gold';
        case 'trafalgar':
            return 'Trafalgar';        
        default:
            return 'Unknown';
    }
};

export const getImageURL = (url, params = { w: 500, h: 500 }) => {
    const paramsObj = {
        smartcrop: 0,
        centrecrop: 1,
        ...params,
        overlay: 0,
    };

    try {
        const urlObj = new URL(url);
        const imageParams = new URLSearchParams(paramsObj).toString();
        return `${urlObj.origin}${urlObj.pathname}?${imageParams}`;
    } catch {
        return 'Invalid URL'
    }
};

export const pluralize = (count, noun, suffix = 's') => `${count} ${noun}${count !== 1 ? suffix : ''}`;