// apiUtils.js
import axios from 'axios';
import handleError from './handleError';

const SERVER_DOMAIN = process.env.NODE_ENV === 'development' ? '//content.local' : process.env.REACT_APP_WP_ROOT;

const getHeaders = () => {
    return {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    };
};

// HTTP GET Request - Returns Resolved or Rejected Promise
export const get = (path) => {
    return new Promise((resolve, reject) => {
      axios.get(`${SERVER_DOMAIN}${path}`, getHeaders())
        .then(response => { resolve(response) })
        .catch(error => { reject(handleError(error)) });
    });
};

// HTTP POST Request - Returns Resolved or Rejected Promise
export const post = (path, data) => {
    return new Promise((resolve, reject) => {
      axios.post(`${SERVER_DOMAIN}${path}`, data, getHeaders())
        .then(response => { resolve(response) })
        .catch(error => { reject(handleError(error)) });
    });
};

// HTTP DELETE Request - Returns Resolved or Rejected Promise
export const del = (path) => {
    return new Promise((resolve, reject) => {
      axios.delete(`${SERVER_DOMAIN}${path}`, getHeaders())
        .then(response => { resolve(response) })
        .catch(error => { reject(handleError(error)) });
    });
};