const brandsArr = [
    {
        slug: 'costsaver',
        name: 'Costsaver'
    },
    {
        slug: 'insightvacations',
        name: 'Insight Vacations'
    },
    {
        slug: 'luxurygold',
        name: 'Luxury Gold'
    },
    {
        slug: 'trafalgar',
        name: 'Trafalgar'
    }
];

export const brands = brandsArr;

export const supportedRegions = [
    { 
        code: 'au',
        currency: 'AUD',
    },
    { 
        code: 'ca',
        currency: 'CAD',
    },
    { 
        code: 'eu',
        currency: 'EUR',
    },
    { 
        code: 'nz',
        currency: 'NZD',
    },
    { 
        code: 'us',
        currency: 'USD',
    }
];

export const defaultRegion = 'us';

export const searchFilters = [
    {
        label: 'Travel Companies',
        name: 'brands',
        items: brandsArr
    },
    {
        label: 'Duration',
        name: 'durationRange',
        items: [
            {
                label: 'Up to 7 days',
                min: 0,
                max: 7
            },
            {
                label: '8 to 14 days',
                min: 8,
                max: 14
            },
            {
                label: '15 to 21 days',
                min: 15,
                max: 21
            },
            {
                label: '22 to 28 days',
                min: 22,
                max: 28
            },
        ]
    },
    {
        label: 'Price',
        name: 'priceRange',
        items: [
            {
                label: 'Under 2000',
                min: 0,
                max: 1999
            },
            {
                label: '2000 - 4000',
                min: 2000,
                max: 3999
            },
            {
                label: '4000 - 6000',
                min: 4000,
                max: 5999
            },
            {
                label: '6000 - 8000',
                min: 6000,
                max: 8000
            },
        ]
    }
];

export const sortOptions = [
    {
        name: 'Price',
        value: 'price',
        order: 'asc'
    },
    {
        name: 'Duration',
        value: 'duration',
        order: 'asc'
    },
    {
        name: 'Tour Name',
        value: 'name',
        order: 'asc'
    }
];