const getParams = () => {
    const paramsObj = Object.fromEntries(new URLSearchParams(window.location.search));
    paramsObj.destination = paramsObj.destination ? paramsObj.destination.split(',').filter(Boolean) : [];
    return paramsObj;
};

export default getParams;