import Cookies from 'js-cookie';
import { defaultRegion, supportedRegions } from './config';

export const useRegion = () => {
    const getRegion = () => { return localStorage.getItem('region') || defaultRegion };

    const currentRegion = supportedRegions.filter(region => region.code === getRegion())[0];

    const setRegion = (region) => {
        const currency = supportedRegions.filter(item => item.code === region)[0].currency.toLowerCase();
        Cookies.set('currency', currency, { expires: 3650, path: '/' });
        localStorage.setItem('region', region);
        window.location.reload();
    }

    return { currentRegion, getRegion, setRegion, supportedRegions };
}