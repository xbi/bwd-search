import React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { sortOptions } from '../utils/config';
import { useRegion } from '../utils/useRegion';
import FeaturedTrip from './featuredTrip';

const TripFinderTours = ({ filters, isLoading, setFilters, singlePath, toggleFilters, tours }) => {
    const { sort } = filters;
    const { currentRegion } = useRegion();

    return (
        <section className="bwd-tours" id="bwd-tours">
            <header className="bwd-tours__header">
                <button className="bwd-tours__filters" type="button" onClick={() => toggleFilters(true)} disabled={isLoading}><span>Filter Tours</span></button>
                <h2>Tours By Quality Brands</h2>
                <div className="bwd-tours-sort">
                    <span className="bwd-tours-sort__label">Sort by:</span>
                    <div className="bwd-tours-sort__dropdown">
                        <button className="bwd-tours-sort__btn" type="button" id="bwd-tours-sort-button" disabled={isLoading}>{sort.name}</button>
                        <div className="bwd-tours-sort__menu" aria-labelledby="bwd-tours-sort-button">
                            {sortOptions.filter(option => option.value !== sort.value).map((option, index) => {
                                return (
                                    <span
                                        className="bwd-tours-sort__item"
                                        key={index}
                                        onClick={() => setFilters({...filters, sort: option })}
                                    >
                                        {option.name}
                                    </span>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </header>
            <TransitionGroup component="ol" className="bwd-tours__items">
                { tours.map(tour => (
                    <CSSTransition
                        key={`${tour.id}_${tour.brand}`}
                        timeout={500}
                        classNames="fade"
                    >
                        <li className="bwd-tours__item">
                            <FeaturedTrip
                                tour={tour}
                                showDepartureDate={true}
                                singlePath={singlePath}
                                currentRegion={currentRegion}
                            />
                        </li>
                    </CSSTransition>
                ))}
            </TransitionGroup>
        </section>
    )
};

export default TripFinderTours;