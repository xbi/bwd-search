import React from 'react';

const withLoading = Component => ({isLoading, ...props}) =>  {
  return (
    <>
      {isLoading && <span>Loading…</span>}
      {!isLoading && <Component {...props} />}
    </>
  )
}

export default withLoading;