import React from 'react';
import { decodeHtmlEntities, formatNumber } from '../utils/filters';
import { useRegion } from '../utils/useRegion';

const Experience = ({ experience }) => {
    const { currentRegion } = useRegion();
    const { _embedded, acf, id, link, title } = experience;
    const { countries, duration } = acf;
    const featuredMedia = _embedded ? _embedded['wp:featuredmedia'][0] : null;
    const decodedTitle = decodeHtmlEntities(title.rendered);
    const price = experience[`price_${currentRegion.code}`];

    return (    
        <article id={`vacations-bwd-${id}`} className="vacations-bwd">
            <header className="entry-header">
                <h2 className="entry-title">
                    <a href={link} rel="bookmark">{decodedTitle}</a>
                </h2>
                <div className="entry-header-info">
                    <ul>
                        <li><strong>Duration:</strong> {duration} days</li>
                        <li><strong>Countries visited:</strong> {countries}</li>
                    </ul>
                    { !!price && <div className="price-bwd">
                        <p>From <span className="amount">{formatNumber(price)}</span> <span className="currency">{currentRegion.currency}</span></p>
                    </div>}
                </div>
            </header>
            <div
                className="entry-img"
                style={{
                    backgroundImage: `url(${featuredMedia.source_url})`
                }}
            />
            <div className="post-thumbnail">
                <img
                    src={featuredMedia.source_url}
                    className="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                    alt={decodedTitle}
                />
            </div>
        </article>
    )
};

export default Experience;