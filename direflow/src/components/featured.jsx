import React from 'react';
import { useRegion } from '../utils/useRegion';
import FeaturedTrip from './featuredTrip';

const Featured = ({ trips, singlePath }) => {
    const { currentRegion } = useRegion();

    return (
        <div className="bwd-f">
            {trips && trips.map((trip, index) => (
                <FeaturedTrip
                    key={index}
                    tour={trip}
                    singlePath={singlePath}
                    currentRegion={currentRegion}
                />
            ))}
        </div>
    );
};

export default Featured;