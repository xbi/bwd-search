import React from 'react';
import Experience from '../components/experience';

const TripFinderExperiences = ({ experiences, experiencesPath }) => {
    return (
        <section className="bwd-experiences" id="bwd-experiences">
            <header className="bwd-experiences__header">
                <h2 className="bwd-experiences__title">BWD Experiences</h2>
                <a
                    className="bwd_experiences__navigation"
                    href={experiencesPath}
                >
                    <span>Go To BWD Experiences</span>
                </a>
            </header>
            <div className="bwd-experiences__content experiences-list">
                {experiences.map((experience) => (
                    <Experience key={experience.id} experience={experience} />
                ))}
            </div>
        </section>
    )
};

export default TripFinderExperiences;
