import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { SwitchTransition, CSSTransition } from 'react-transition-group';
import { pluralize } from '../utils/filters';

const TripFinderHeader = ({ destination = [], isLoading, totalExperiences, totalTrips }) => {
    const loadingText = 'Searching for Bwd Experiences & Other Trips…';

    const getSummary = useMemo(() => totalExperiences > 0 ? (
        <>
            <span>Found </span>
            <a href="#bwd-experiences">{pluralize(totalExperiences, 'Bwd Experience')}</a>
            <span> And </span>
            <a href="#bwd-tours">{pluralize(totalTrips, 'Other Trip')}</a>
            { destination.length > 0 && <span>{` For “${destination.join(', ')}”`}</span> }
        </>
    ) : (
        <>
            <a href="#bwd-tours">{pluralize(totalTrips, 'Trip')}</a>
            <span> Found</span>
            { destination.length > 0 && <span>{` For “${destination.join(', ')}”`}</span> }
        </>        
    ), [destination, totalExperiences, totalTrips]);

    return (
        <SwitchTransition mode={'out-in'}>
            <CSSTransition key={isLoading} timeout={500} classNames="fade">
                <header className="bwd-tf-summary">
                    { isLoading && <h1 className="bwd-tf-summary__text">{loadingText}</h1> }
                    { !isLoading && <h1 className="bwd-tf-summary__text">{ getSummary }</h1> }
                </header>
            </CSSTransition>
        </SwitchTransition>
    )
}

TripFinderHeader.propTypes = {
    destination: PropTypes.arrayOf(PropTypes.string).isRequired,
    isLoading: PropTypes.bool.isRequired,
    totalExperiences: PropTypes.number.isRequired,
    totalTrips: PropTypes.number.isRequired
};

export default TripFinderHeader;