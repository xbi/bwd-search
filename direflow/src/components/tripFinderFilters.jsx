import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import DatePicker, { registerLocale } from "react-datepicker";
import en from 'date-fns/locale/en-GB';
import { searchFilters } from '../utils/config';
import { formatDate, pluralize } from '../utils/filters';
import { useRegion } from '../utils/useRegion';

// Set locale for the datepicker component
registerLocale('en-GB', en);

const Filters = ({ totalResults, filters, setFilters, showFilters, toggleFilters }) => {
    const { currentRegion } = useRegion();
    const [fromDate, setFromDate] = useState(null);
    const [toDate, setToDate] = useState(null);

    const handleChange = ({ target, filter, min, max, slug, date }) => {
        let newFilter;

        if (filter === 'from' || filter === 'to') {
            newFilter = formatDate(date, 'YYYY-MM-DD');
            if (filter === 'from') setFromDate(date ? new Date(date) : null);
            if (filter === 'to') setToDate(date ? new Date(date) : null);
        } else {
            if (slug === null) {
                newFilter = target.checked ? [...filters[filter], [min,max]] : [...filters[filter].filter(item => item[0] !== min && item[1] !== max)];
            } else {
                newFilter = target.checked ? [...filters[filter], slug] : [...filters[filter].filter(item => item !== slug)];
            }
        }

        setFilters({...filters, [filter]: newFilter });
    };

    return (
        <aside className={cx('bwd-ft', { 'bwd-ft--active': showFilters })}>
            <header className="bwd-ft__header">
                <p className="bwd-ft__title">Filters</p>
                <button
                    type="button"
                    className="bwd-ft__close"
                    onClick={() => toggleFilters(false)}
                >
                    <span className="sr-only">Close</span>
                </button>
            </header>
            <div className="bwd-ft__inner">
                { searchFilters && searchFilters.map((filter, i) => (
                    <fieldset className="bwd-ft__fieldset" key={i}>
                        <legend className="bwd-ft__legend">{filter.label}</legend>
                        { filter.items && filter.items.map((item,j) => (
                            <div className="bwd-ft__item" key={j}>
                                <input
                                    type="checkbox"
                                    className="bwd-ft__checkbox"
                                    name={`bwd-filter-${i}-${j}`}
                                    id={`bwd-filter-${i}-${j}`}
                                    onChange={e => handleChange({
                                        target: e.target,
                                        filter: filter.name,
                                        min: item.min || null,
                                        max: item.max || null,
                                        slug: item.slug || null
                                    })}
                                />
                                <label
                                    className="bwd-ft__label"
                                    htmlFor={`bwd-filter-${i}-${j}`}
                                >
                                    {item.name && item.name}
                                    {item.label && item.label} {filter.label === 'Price' && currentRegion.currency}
                                </label>
                            </div>
                        ))}
                    </fieldset>
                ))}
                <fieldset>
                    <legend className="bwd-ft__legend">From</legend>
                    <DatePicker
                        locale="en-GB"
                        minDate={new Date()}  
                        onChange={date => handleChange({ filter: 'from', date: date })}
                        placeholderText="First departure date"
                        selected={fromDate}
                        selectsStart
                        startDate={fromDate}
                        endDate={toDate}
                        dateFormat="dd/MM/yyyy"
                        isClearable
                    />
                </fieldset>
                <fieldset>
                    <legend className="bwd-ft__legend">To</legend>
                    <DatePicker
                        locale="en-GB"
                        onChange={date => handleChange({ filter: 'to', date: date })}
                        placeholderText="Last departure date"
                        selected={toDate}
                        selectsEnd
                        startDate={fromDate}
                        endDate={toDate}
                        minDate={fromDate}
                        dateFormat="dd/MM/yyyy"
                        isClearable
                    />                
                </fieldset>
                <button
                    type="button"
                    className="bwd-ft__show"
                    onClick={() => toggleFilters(false)}
                >
                    <span>{`Show ${pluralize(totalResults, 'result')}`}</span>
                </button>
            </div>
        </aside>
    )
};

Filters.propTypes = {
    filters: PropTypes.shape({
        brands: PropTypes.arrayOf(PropTypes.string),
        date: PropTypes.string,
        destination: PropTypes.arrayOf(PropTypes.string),
        durationRange: PropTypes.array,
        priceRange: PropTypes.array,
        sort: PropTypes.shape({
            name: PropTypes.string,
            value: PropTypes.string,
            order: PropTypes.string
        })        
    }).isRequired,
    setFilters: PropTypes.func.isRequired,
    showFilters: PropTypes.bool.isRequired,
    toggleFilters: PropTypes.func.isRequired,
    totalResults: PropTypes.number.isRequired
};

export default Filters;