import React, { useCallback, useRef, useState } from 'react';
import { formatDate, formatNumber, getBrand, getImageURL } from '../utils/filters';

const targetImageSize = 530;

const FeaturedTrip = ({ currentRegion, tour, showDepartureDate = false, singlePath }) => {
    const { brand, content, extraImages, id, name } = tour;
    const { durationMin, firstDepartureDate, lastDepartureDate, fromPrice, photo, startPlace } = content[0];
    const [imageUrl, setImageUrl] = useState(() => getImageURL(photo, { w: 530, h: 530 }));
    const imgRef = useRef();

    const handleLoadImage = useCallback(() => {
        const w = imgRef.current.naturalWidth;
        if (w < targetImageSize && extraImages.length > 0) {
            const newImageUrls = Object.values(extraImages[0]).reduce((arr, image) => {
                if ( ( image.width >= targetImageSize ) && ( image.width === image.height ) ) {
                    arr = [ ...arr, image.url];
                }
                return arr;
            }, []).sort((a, b) => a.width - b.width);
            if (newImageUrls.length > 0) {
                setImageUrl(getImageURL(newImageUrls[0], { w: targetImageSize, h: targetImageSize }));
            }
        }
    }, [extraImages]); 

    return (
        <article className="bwd-f-trip">
            <div className="bwd-f-trip__pic">
                <span className="bwd-f-trip__brand">{getBrand(brand)}</span>
                <a href={`${singlePath}/?b=${brand}&t=${id}&r=${currentRegion.currency.toLowerCase()}`} rel="bookmark">
                    <img ref={imgRef} src={imageUrl} alt={name} className="bwd-f-trip__photo" onLoad={handleLoadImage} />
                </a>
            </div>
            <div className="bwd-f-trip__content">
                <h3 className="bwd-f-trip__name">
                    <a href={`${singlePath}/?b=${brand}&t=${id}&r=${currentRegion.currency.toLowerCase()}`} rel="bookmark">{name}</a>
                </h3>
                <div className="bwd-f-trip__meta">
                    <span className="country">{startPlace.country}</span>
                    { showDepartureDate && (
                        <>
                            <span className="firstDeparture"><span>First Departure:</span> <time className="date" dateTime={firstDepartureDate}>{formatDate(firstDepartureDate)}</time></span>
                            <span className="lastDeparture"><span>Last Departure:</span> <time className="date" dateTime={lastDepartureDate}>{formatDate(lastDepartureDate)}</time></span>
                        </>
                    )}
                    <span className="duration">{durationMin} days</span>
                    <span className="price">From {formatNumber(fromPrice.price.adultPrice)} {fromPrice.currency}</span>
                </div>
            </div>
        </article>
    );
};

export default FeaturedTrip;