# BWD Search

- Contributors: xbonell
- Requires at least: 3.0.1
- Tested up to: 5.5.1
- Stable tag: 4.3
- License: GPLv2 or later
- License URI: http://www.gnu.org/licenses/gpl-2.0.html

## Description

This plugin provides trip finder functionality for the <a href="https://www.travelbwd.com" target="_blank" rel="noopener nofollow">Travel BWD</a> website.