<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://xbonell.com
 * @since      1.0.0
 *
 * @package    Bwd_Search
 * @subpackage Bwd_Search/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Bwd_Search
 * @subpackage Bwd_Search/public
 * @author     Xavier Bonell <mail@xbonell.com>
 */
class Bwd_Search_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bwd_Search_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bwd_Search_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bwd-search-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bwd_Search_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bwd_Search_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bwd-search-public.js', array( 'jquery' ), $this->version, false );

	}

}

function bwd_featured_shortcode() {
	$singlePath = isset(get_option( 'bwd_search_plugin_options' )['single_path']) ? ' singlePath="' . get_option( 'bwd_search_plugin_options' )['single_path'] . '"' : '';
	return '<bwd-featured' . $singlePath . '></bwd-featured>';
}

add_shortcode('bwd_featured', 'bwd_featured_shortcode');

function bwd_form_shortcode() {
	$searchPath = isset(get_option( 'bwd_search_plugin_options' )['search_path']) ? ' searchPath="' . get_option( 'bwd_search_plugin_options' )['search_path'] . '"' : '';
	$showDatepicker = isset(get_option( 'bwd_search_plugin_options' )['show_datepicker']) &&  ( get_option( 'bwd_search_plugin_options' )['show_datepicker'] == 1 )? ' showDatepicker="true"' : '';
	return '<bwd-form' . $searchPath . $showDatepicker . '></bwd-form>';
}

add_shortcode('bwd_form', 'bwd_form_shortcode');

function bwd_region_select_shortcode() {
	$rootPath = isset(get_option( 'bwd_search_plugin_options' )['root_path']) ? ' rootPath="' . get_option( 'bwd_search_plugin_options' )['root_path'] . '"' : '';
	return '<bwd-region-select' . $rootPath . '></bwd-region-select>';
}

add_shortcode('bwd_region_select', 'bwd_region_select_shortcode');

function bwd_trip_finder_shortcode() {
	$experiencesPath = isset(get_option( 'bwd_search_plugin_options' )['experiences_path']) ? ' experiencesPath="' . get_option( 'bwd_search_plugin_options' )['experiences_path'] . '"' : '';
	$singlePath = isset(get_option( 'bwd_search_plugin_options' )['single_path']) ? ' singlePath="' . get_option( 'bwd_search_plugin_options' )['single_path'] . '"' : '';
	return '<bwd-trip-finder' . $experiencesPath . $singlePath . '></bwd-trip-finder>';
}

add_shortcode('bwd_trip_finder', 'bwd_trip_finder_shortcode');
