<?php
/**
 * Template Name: Get Countries
 *
 * Get country list from the TTC API.
 *
 * @package Bwd_Search
 */

// Our include
define('WP_USE_THEMES', false);
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

function getDestinations() {
  $force = ( isset($_GET['action']) && ( $_GET['action'] == 'refresh' ) );
  $region = isset($_GET['region']) ? $_GET['region'] : 'us';
  $transient = get_transient( 'bwd_destinations' );
  $transientTrips = get_transient( 'bwd_trips_' . $region );

  if( !empty( $transient ) && !$force ) {
    return $transient;
  } else { 
    $args = array(
      'headers' => array(
        'Authorization' => 'Basic ' . base64_encode('token:' . get_option( 'bwd_search_plugin_options' )['api_token']),
        'Accept' => 'application/vnd.ttc.v4+json'
      )
    );

    $request = wp_remote_get('https://api.ttc.com/countries', $args);

    if( is_wp_error($request) ){
      return false;
    }

    $body = wp_remote_retrieve_body($request);

    if ( !empty( $transientTrips ) ) {
      $trips = json_decode($transientTrips, TRUE);
      $countries = json_decode($body, TRUE);
      $locationsVisited = array();
      $cities = array();

      foreach ($trips as $trip) {
        foreach ($trip['locationsVisited'] as $location) {
          array_push( $cities, array( 'name' => $location ) );
        }
        unset($location);
      }
      unset($trip);

      $cities = array_unique($cities, SORT_REGULAR);
      $countries = array_merge($countries, $cities);

      $body = json_encode($countries);
    }

    set_transient( 'bwd_destinations', $body, 86400 );   // Save the API response so we don't have to call again until later (600 seconds).
    return $body;
  }
}

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
echo getDestinations();

