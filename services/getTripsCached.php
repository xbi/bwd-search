<?php
/**
 * Template Name: Get Trips FULL
 *
 * Get all trips from the selected brands and merge them into one data set with enhanced info.
 *
 * @package Bwd_Search
 */

// Our include
define('WP_USE_THEMES', false);
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

// include file
require_once(dirname(plugin_dir_path(__FILE__)) . '/includes/getTripsInc.php');

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
echo getTripsCached();

