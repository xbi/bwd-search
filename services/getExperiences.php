<?php
/**
 * Template Name: Get Experiences
 *
 * Get all experiences.
 *
 * @package Bwd_Search
 */

// Our include
define('WP_USE_THEMES', false);
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

function array_push_assoc($array, $key, $value) {
  $array[$key] = $value;
  return $array;
}

function get_experiences() {
    $transient = get_transient( 'bwd_experiences' );

    if( ! empty( $transient ) ) {
      return $transient;
    } else {
      $currencies = array(
        'AUD' => 'au',
        'CAD' => 'ca',
        'EUR' => 'eu',
        'NZD' => 'nz',
        'USD' => 'us'
      );

      $args = array(
        'headers' => array(
          'Accept' => 'application/json'
        )
      );
  
      $request = wp_remote_get( get_site_url() . '/wp-json/wp/v2/vacations-bwd?_embed', $args);
  
      if( is_wp_error($request) ){
        return false;
      } else {
        $body = wp_remote_retrieve_body($request);
      }
  
      $exchangerate_url = 'https://api.exchangerate-api.com/v4/latest/EUR';
      $exchangerate_json = wp_remote_get($exchangerate_url, $args);

      if (is_wp_error($exchangerate_json)) {
        $body = wp_remote_retrieve_body($request);
      } else {

        try {
          $exchangerate_object = json_decode(wp_remote_retrieve_body($exchangerate_json));
          $decodedExperiences = json_decode($body, TRUE);

          $i=0;

          foreach ($decodedExperiences as $experience) {

            foreach ($currencies as $currency => $code) {
              $rate = $exchangerate_object->rates->$currency;
              $price = round( ( floatval($experience['acf']['price']) * $rate ), 2);
              $experience = array_push_assoc($experience, 'price_' . $code, $price);
            }

            unset($currency);
            $decodedExperiences[$i] = $experience;
            $i++;
          }

          unset($experience);
          $body = json_encode($decodedExperiences);

        } catch(Exception $e) {
          return false;
        }

      }
  
      set_transient( 'bwd_experiences', $body, 600 );   // Save the API response so we don't have to call again until later (600 seconds).
      return $body;
    }
}

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
echo get_experiences();